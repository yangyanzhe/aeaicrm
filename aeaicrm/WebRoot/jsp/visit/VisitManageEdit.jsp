<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://www.agileai.com" prefix="aeai"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>拜访记录</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script language="javascript">
var visitCustIdBox;
function openVisitCustIdBox(){
	var handlerId = "VisitListSelectList";
	if (!visitCustIdBox){
		visitCustIdBox = new PopupBox('visitCustIdBox','请选择客户',{size:'normal',width:'300',top:'2px'});
	}
	var url = 'index?'+handlerId+'&targetId=VISIT_CUST_ID&targetName=VISIT_CUST_ID_NAME';
	visitCustIdBox.sendRequest(url);
}
function stateConfirm(){
	doSubmit({actionType:'confirm'});
	parent.refreshContent();
}
function stateConfirmCounter(){
	doSubmit({actionType:'confirmCounter'});
	parent.refreshContent();
}

function dosave(){
	if(!parseFloat(ele('VISIT_COST').value)==0){
		requiredValidator.add("VISIT_COST_EXPLAIN");
	}
	doSubmit({actionType:'save'})
}
function doCreateClueAction(){
	doSubmit({actionType:'doCreateClueAction'});
}
var contIdBox;
function openContIdBox(){
	var handlerId = "ContListSelectList";
	if (!contIdBox){
		contIdBox = new PopupBox('contIdBox','请选择联系人',{size:'normal',height:'500px',width:'700px',top:'2px'});  
	}
	var url = 'index?'+handlerId+'&targetId=VISIT_RECEPTION_NAME&targetName=VISIT_RECEPTION&custId='+$('#VISIT_CUST_ID').val();
	contIdBox.sendRequest(url);
} 
var editBox;
function showEditBox(){
	clearSelection();
	var handlerId = "VisitCreateContEdit"; 
	if (!editBox){
		editBox = new PopupBox('editBox','新增联系人',{size:'big',height:'350px',width:'600px',top:'30px'});
	}
	var url = 'index?'+handlerId+'&custId='+ele('VISIT_CUST_ID').value+'&operaType=insert';
	editBox.sendRequest(url);
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ParamBar__" style="float: right;">&nbsp;</div>
<div id="__ToolBar__">
<table border="0" cellpadding="0" cellspacing="1">
<tr>
<%if(pageBean.getBoolValue("doDetail")){%>
   <aeai:previlege code="edit"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="enableSave()" ><input value="&nbsp;" type="button" class="editImgBtn" id="modifyImgBtn" title="编辑" />编辑</td></aeai:previlege>
   <aeai:previlege code="save"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="dosave()"><input value="&nbsp;" type="button" class="saveImgBtn" id="saveImgBtn" title="保存" />保存</td></aeai:previlege>
<%}%>
<%if(pageBean.getBoolValue("doCreateCLue")){%>
   <aeai:previlege code="generateClue"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="doCreateClueAction()" ><input value="&nbsp;"type="button" class="businessImgBtn" id="revokeApproval" title="生成商机" />生成商机</td></aeai:previlege>
<%}%>
<%if(pageBean.getBoolValue("doConfirm")){%>
   <aeai:previlege code="confirm"><td  align="center"class="bartdx"onclick="stateConfirm();" onmouseover="onMover(this);" onmouseout="onMout(this);"><input value="&nbsp;"type="button" class="submitImgBtn" title="提交" />提交</td></aeai:previlege>
<%}%>
   <aeai:previlege code="back"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="goToBack();"><input value="&nbsp;" type="button" class="backImgBtn" title="返回" />返回</td></aeai:previlege>
</tr>
</table>
</div>
<%if(pageBean.getBoolValue("displayTable")){%>
<table class="detailTable" cellspacing="0" cellpadding="0">
<tr>
	<th width="100" nowrap>拜访日期</th>
	<td><input id="PROCUST_VISIT_DATE" label="拜访日期" name="PROCUST_VISIT_DATE" type="text" value="<%=pageBean.inputDate("PROCUST_VISIT_DATE")%>" size="24" class="text" readonly="readonly"/>
</td>
</tr>
<tr>
	<th width="100" nowrap>拜访类型</th>
    <td><input id="PROCUST_VISIT_TYPE_TEXT" label="拜访类型" name="PROCUST_VISIT_TYPE_TEXT" type="text" value="<%=pageBean.selectedText("PROCUST_VISIT_TYPE")%>" size="24"  class="text" readonly="readonly"/>
	<input id="PROCUST_VISIT_TYPE" label="拜访类型" name="PROCUST_VISIT_TYPE" type="hidden" value="<%=pageBean.selectedValue("PROCUST_VISIT_TYPE")%>" />
</td>
</tr>
<tr>
	<th width="100" nowrap>拜访效果</th>
    <td><input id="PROCUST_VISIT_EFFECT_TEXT" label="拜访效果" name="PROCUST_VISIT_EFFECT_TEXT" type="text" value="<%=pageBean.selectedText("PROCUST_VISIT_EFFECT")%>" size="24"  class="text" readonly="readonly"/>
	<input id="PROCUST_VISIT_EFFECT" label="拜访效果" name="PROCUST_VISIT_EFFECT" type="hidden" value="<%=pageBean.selectedValue("PROCUST_VISIT_EFFECT")%>" />
</td>
</tr>
<tr>
	<th width="100" nowrap>填写人</th>
	<td><input id="PROCUST_VISIT_FILL_NAME" label="填写人" name="PROCUST_VISIT_FILL_NAME" type="text" value="<%=pageBean.inputValue("PROCUST_VISIT_FILL_NAME")%>" size="24" class="text" readonly="readonly"/>
</td>
</tr>
<tr>
	<th width="100" nowrap>填写时间</th>
	<td><input id="PROCUST_VISIT_FILL_TIME" label="填写时间" name="PROCUST_VISIT_FILL_TIME" type="text" value="<%=pageBean.inputTime("PROCUST_VISIT_FILL_TIME")%>" size="24" class="text" readonly="readonly"/>
</td>
</tr>
<tr>
	<th width="100" nowrap>拜访备注</th>
	<td><textarea id="PROCUST_VISIT_REMARK" label="拜访备注" name="PROCUST_VISIT_REMARK" cols="40" rows="3" class="textarea" readonly="readonly"><%=pageBean.inputValue("PROCUST_VISIT_REMARK")%></textarea>
</td>
</tr>
<tr>
	<th width="100" nowrap>客户关注点</th>
	<td><textarea id="PROCUST_VISIT_CUST_FOCUS" label="客户关注点" name="PROCUST_VISIT_CUST_FOCUS" cols="40" rows="3" class="textarea" readonly="readonly"><%=pageBean.inputValue("PROCUST_VISIT_CUST_FOCUS")%></textarea>
</td>
</tr>
</table>
<%}else{%>
<table class="detailTable" cellspacing="0" cellpadding="0">
<tr>
<!-- 	<th width="100" nowrap>客户名称</th> -->
<%-- 	<td width="348"><input name="VISIT_CUST_ID_NAME" type="text" class="text" id="VISIT_CUST_ID_NAME" value="<%=pageBean.inputValue("VISIT_CUST_ID_NAME")%>" size="24" readonly="readonly" label="填写人"  /> --%>
<!-- 	<img src="images/sta.gif" alt="" width="16" height="16" id="visitCustIdSelectImage" onclick="openVisitCustIdBox()" /></td> -->
	<th width="100" nowrap>拜访类型</th>
	<td width="300"><select id="VISIT_TYPE" label="拜访类型" name="VISIT_TYPE" class="select">
	  <%=pageBean.selectValue("VISIT_TYPE")%>
	  </select></td>
	<th width="100" nowrap>接待人姓名</th>
	<td>
<%-- 		<input id="VISIT_RECEPTION_NAME" label="接待人姓名" name="VISIT_RECEPTION_NAME" type="text" value="<%=pageBean.inputValue("VISIT_RECEPTION_NAME")%>" size="24" class="text" /> --%>
	<input id="VISIT_RECEPTION" label="联系人姓名" name="VISIT_RECEPTION" type="text" value="<%=pageBean.inputValue("VISIT_RECEPTION")%>" readonly="readonly" size="24" class="text" />
	 <%if(!pageBean.getBoolValue("readOnly")){ %>
	<img id="contIdSelectImage" src="images/sta.gif" width="16" height="16" onclick="openContIdBox()" />
<!-- 	<label onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="showEditBox()"> -->
<!-- 	<input value="&nbsp;" type="button" class="editImgBtn" title="新增" />新增</label> -->
	<%} %>
	<input id="VISIT_RECEPTION_NAME" name="VISIT_RECEPTION_NAME" type="hidden" value="<%=pageBean.inputValue("VISIT_RECEPTION_NAME")%>"/>
</td>
	</td>
</tr>
<tr>
<tr>
	<th width="100" nowrap>拜访人员</th>
	<td>
	  <input name="VISIT_USER_NAME" type="text" class="text" id="VISIT_USER_NAME" value="<%=pageBean.inputValue("VISIT_USER_NAME")%>" size="24" label="拜访人员" />
	  <input id="VISIT_USER_ID" label="拜访人员" name="VISIT_USER_ID" type="hidden" value="<%=pageBean.inputValue("VISIT_USER_ID")%>" size="24" class="text" hidden="hidden" />
      </td>
	<th width="100" nowrap>同行人员</th>
	<td><input id="VISIT_PEER_NAME" label="同行人员" name="VISIT_PEER_NAME" type="text" value="<%=pageBean.inputValue("VISIT_PEER_NAME")%>" size="24" class="text" /></td>
</tr>
<tr>
    <th width="100" nowrap>拜访时间</th>
	<td><input name="VISIT_DATE" type="text" class="text" id="VISIT_DATE" value="<%=pageBean.inputTime("VISIT_DATE")%>" size="24" readonly="readonly" label="拜访日期" />
	  <img id="VISIT_DATEPicker" src="images/calendar.gif" width="16" height="16" alt="日期/时间选择框" /></td>
	<th width="100" nowrap>拜访费用</th>
	<td><input id="VISIT_COST" label="拜访费用" name="VISIT_COST" type="text" value="<%=pageBean.inputValue("VISIT_COST")%>" size="24" maxlength="6" class="text" /></td>    
</tr>
<tr>
	<th width="100" nowrap>沟通效果</th>
	<td><select id="VISIT_EFFECT" label="沟通效果" name="VISIT_EFFECT" style="width:213px" class="select"><%=pageBean.selectValue("VISIT_EFFECT")%></select></td>
	<th width="100" nowrap>状态</th>
	<td><input id="VISIT_STATE_TEXT" label="状态" name="VISIT_STATE_TEXT" type="text" value="<%=pageBean.selectedText("VISIT_STATE")%>" size="24"  class="text" readonly="readonly"/>
	<input id="VISIT_STATE" label="状态" name="VISIT_STATE" type="hidden" value="<%=pageBean.selectedValue("VISIT_STATE")%>" />
</td>
</tr>
<tr>
	<th width="100" nowrap>费用说明</th>
	<td colspan="3">
	  <textarea id="VISIT_COST_EXPLAIN" label="费用说明" name="VISIT_COST_EXPLAIN" cols="104" rows="3" class="textarea"><%=pageBean.inputValue("VISIT_COST_EXPLAIN")%></textarea>
	  </td>
</tr>
<tr>
	<th width="100" nowrap>交互内容</th>
	<td colspan="3"><textarea id="VISIT_CONTENT" label="交互内容" name="VISIT_CONTENT" cols="104" rows="3" class="textarea"><%=pageBean.inputValue("VISIT_CONTENT")%></textarea>
</td>
</tr>
<tr>
	<th width="100" nowrap>客户关注点</th>
	<td colspan="3"><textarea id="VISIT_CUST_FOCUS" label="交互内容" name="VISIT_CUST_FOCUS" cols="104" rows="3" class="textarea"><%=pageBean.inputValue("VISIT_CUST_FOCUS")%></textarea>
</td>
</tr>

<tr>
	<th width="100" nowrap>待改进情况</th>
	<td colspan="3">
	  <textarea name="VISIT_IMPROVEMENT" cols="104" rows="3" class="text" id="VISIT_IMPROVEMENT" label="待改进情况"><%=pageBean.inputValue("VISIT_IMPROVEMENT")%></textarea></td>
</tr>
<tr>
	<th width="100" nowrap>填写人</th>
	<td><input name="VISIT_FILL_NAME" type="text" class="text" id="VISIT_FILL_NAME" value="<%=pageBean.inputValue("VISIT_FILL_NAME")%>" size="24" readonly="readonly" label="填写人" />
    <input id="VISIT_FILL_ID" label="填写人" name="VISIT_FILL_ID" type="hidden" value="<%=pageBean.inputValue("VISIT_FILL_ID")%>" size="24" class="text" hidden="hidden" />
    
&nbsp;&nbsp;</td>
	<th width="100" nowrap>填写时间</th>
	<td><input id="VISIT_FILL_TIME" label="填写时间" name="VISIT_FILL_TIME" type="text" value="<%=pageBean.inputTime("VISIT_FILL_TIME")%>" size="24" class="text"  /></td>
</tr>
<tr>
	<th width="100" nowrap>确认人</th>
	<td><input name="VISIT_CONFIRM_NAME" type="text" class="text" id="VISIT_CONFIRM_NAME" value="<%=pageBean.inputValue("VISIT_CONFIRM_NAME")%>" size="24" readonly="readonly" label="填写人" />
      <input id="VISIT_CONFIRM_ID" label="确认人" name="VISIT_CONFIRM_ID" type="hidden" value="<%=pageBean.inputValue("VISIT_CONFIRM_ID")%>" size="24" class="text" />
&nbsp;</td>
	<th width="100" nowrap>确认时间&nbsp;</th>
	<td><input id="VISIT_CONFIRM_TIME" label="确认时间" name="VISIT_CONFIRM_TIME" type="text" value="<%=pageBean.inputTime("VISIT_CONFIRM_TIME")%>" size="24" class="text" /></td>
</tr>
</table>
<%} %>
<input type="hidden" name="actionType" id="actionType" value=""/>
<input type="hidden" name="operaType" id="operaType" value="<%=pageBean.getOperaType()%>"/>
<input type="hidden" id="VISIT_ID" name="VISIT_ID" value="<%=pageBean.inputValue("VISIT_ID")%>" />
<input type="hidden" name="custId" id="custId" value="<%=pageBean.inputValue("custId")%>" />
<input type="hidden" name="VISIT_CUST_ID" id="VISIT_CUST_ID" value="<%=pageBean.inputValue("VISIT_CUST_ID")%>" />
<input type="hidden" id="PROCUST_VISIT_ID" name="PROCUST_VISIT_ID" value="<%=pageBean.inputValue4DetailOrUpdate("PROCUST_VISIT_ID","")%>" />
<input type="hidden" id="ORG_ID" name="ORG_ID" value="<%=pageBean.inputValue("ORG_ID")%>" />
<input type="hidden" id="CUST_VISIT_CATEGORY" name="CUST_VISIT_CATEGORY" value="<%=pageBean.selectedValue("CUST_VISIT_CATEGORY")%>" />
</form>
<script language="javascript">
$('#VISIT_COST_EXPLAIN').inputlimiter({
	limit: 100,
	remText: '还可以输入  %n 字 /',
	limitText: '%n 字',
	zeroPlural: false
	});
$('#VISIT_CONTENT').inputlimiter({
	limit: 100,
	remText: '还可以输入  %n 字 /',
	limitText: '%n 字',
	zeroPlural: false
	});
$('#VISIT_CUST_FOCUS').inputlimiter({
	limit: 100,
	remText: '还可以输入  %n 字 /',
	limitText: '%n 字',
	zeroPlural: false
	});
$('#VISIT_IMPROVEMENT').inputlimiter({
	limit: 100,
	remText: '还可以输入  %n 字 /',
	limitText: '%n 字',
	zeroPlural: false
	});
<%if(!pageBean.getBoolValue("displayTable")){%>
initCalendar('VISIT_DATE','%Y-%m-%d %H:%m','VISIT_DATEPicker');
<%} %>
datetimeValidators[0].set("yyyy-MM-dd HH:mm").add("VISIT_DATE");
datetimeValidators[1].set("yyyy-MM-dd HH:mm").add("VISIT_FILL_TIME");
datetimeValidators[2].set("yyyy-MM-dd HH:mm").add("VISIT_CONFIRM_TIME");
initDetailOpertionImage();
datetimeValidators[0].set("yyyy-MM-dd").add("PROCUST_VISIT_DATE");
datetimeValidators[1].set("yyyy-MM-dd HH:mm").add("PROCUST_VISIT_FILL_TIME");
numValidator.add("VISIT_COST");
requiredValidator.add("VISIT_DATE");
requiredValidator.add("VISIT_EFFECT");
requiredValidator.add("VISIT_TYPE");
requiredValidator.add("VISIT_RECEPTION");
</script>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
