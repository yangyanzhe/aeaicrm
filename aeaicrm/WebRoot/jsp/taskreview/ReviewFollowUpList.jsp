<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://www.ecside.org" prefix="ec"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>意向跟进</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script type="text/javascript">
var operaRequestBox;
function openContentRequestBox(operaType,title,handlerId,subPKField){
	if (!isSelectedRow()){
		writeErrorMsg('请先选中一条记录!');
		return;
	}
	if (!operaRequestBox){
		operaRequestBox = new PopupBox('operaRequestBox',title,{size:'big',height:'600px',width:'1050',top:'3px',scroll:'yes'});
	}
	var url = 'index?'+handlerId+'&operaType='+operaType+'&'+subPKField+'='+$("#"+subPKField).val();
	operaRequestBox.sendRequest(url);
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ToolBar__">
<table class="toolBar" border="0" cellpadding="0" cellspacing="1">
<tr>
<%if ("SubmitPlan".equals(pageBean.inputValue("TASK_REVIEW_STATE"))){%>
   	<td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="doSubmit({actionType:'confirmPlan'})"><input value="&nbsp;" type="button" class="confirmImgBtn" title="确认计划" />确认计划</td>
	<td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="doSubmit({actionType:'repulsePlan'})"><input value="&nbsp;" type="button" class="removeImgBtn" title="打回计划" />打回计划</td>
<%} %>   
</tr>
</table>
</div>
<ec:table 
form="form1"
var="row"
items="pageBean.rsList" csvFileName="意向跟进.csv"
retrieveRowsCallback="process" xlsFileName="意向跟进.xls"
useAjax="true" sortable="true"
doPreload="false" toolbarContent="navigation|pagejump |pagesize |extend|status"
width="100%" rowsDisplayed="15"
listWidth="100%" 
height="390px"
>
<ec:row styleClass="odd" ondblclick="openContentRequestBox('detail','客户信息','ReviewFollowUpEdit','CUST_ID')" oncontextmenu="selectRow(this,{CUST_ID:'${row.CUST_ID}'});refreshConextmenu()" onclick="selectRow(this,{CUST_ID:'${row.CUST_ID}'})">
	<ec:column width="50" style="text-align:center" property="_0" title="序号" value="${GLOBALROWCOUNT}" />
	<ec:column width="100" property="CUST_NAME" title="名称"   />
	<ec:column width="100" property="CUST_INDUSTRY" title="行业"   mappingItem="CUST_INDUSTRY"/>
	<ec:column width="100" property="CUST_SCALE" title="规模"   mappingItem="CUST_SCALE"/>
	<ec:column width="100" property="CUST_NATURE" title="性质"   mappingItem="CUST_NATURE"/>
	<ec:column width="100" property="CUST_STATE" title="状态"   mappingItem="CUST_STATE"/>
	<ec:column width="120" property="TASK_FOLLOW_STATE" title="跟进状态" mappingItem="TASK_FOLLOW_STATE" />
</ec:row>
</ec:table>
<input type="hidden" name="TASK_ID" id="TASK_ID" value="" />
<input type="hidden" name="CUST_ID" id="CUST_ID" value="" />
<input type="hidden" name="actionType" id="actionType" />
<input type="hidden" name="TASK_REVIEW_ID" id="TASK_REVIEW_ID" value="<%=pageBean.inputValue("TASK_REVIEW_ID")%>" />
<input type="hidden" name="TC_ID" id="TC_ID" value="<%=pageBean.inputValue("TC_ID")%>" />
<input type="hidden" name="SALE_ID" id="SALE_ID" value="<%=pageBean.inputValue("SALE_ID")%>" />
<input type="hidden" name="ids" id="ids" value="<%=pageBean.inputValue("ids")%>" />
<script language="JavaScript">
setRsIdTag('CUST_ID');
var ectableMenu = new EctableMenu('contextMenu','ec_table');
</script>
</form>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
