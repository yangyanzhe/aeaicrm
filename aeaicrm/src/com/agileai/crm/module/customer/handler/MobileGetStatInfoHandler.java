package com.agileai.crm.module.customer.handler;

import java.util.List;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

import com.agileai.crm.cxmodule.CustomerGroup8ContentManage;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.controller.core.BaseHandler;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.util.DateUtil;

public class MobileGetStatInfoHandler extends BaseHandler  {

	@PageAction
	public ViewRenderer getStatInfo(DataParam param){
		String responseText = FAIL;
		try {
			CustomerGroup8ContentManage customerGroup8ContentManage = this.lookupService(CustomerGroup8ContentManage.class);
			List<DataRow> records = customerGroup8ContentManage.getStatInfo();
			JSONArray jsonArray1 = new JSONArray();
			JSONArray oppNums = new JSONArray();
			JSONArray orderNums = new JSONArray();
			for(int i=0;i<records.size();i++){
				DataRow row = records.get(i);
				jsonArray1.put(i, row.getString("month"));
				oppNums.put(i, row.get("oppNums"));
				orderNums.put(i, row.get("orderNums"));
			}
			JSONObject jsonObject = new JSONObject();
			JSONArray jsonArray = new JSONArray();
			jsonArray.put(0, oppNums);
			jsonArray.put(1, orderNums);
			JSONArray jsonArray2 = new JSONArray();
			jsonArray2.put(0, "商机个数");
			jsonArray2.put(1, "订单个数");
			jsonObject.put("labels",jsonArray1);
			jsonObject.put("series",jsonArray2);
			jsonObject.put("data",jsonArray);
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer getStatInfoSheet(DataParam param){
		String responseText = FAIL;
		try {
			CustomerGroup8ContentManage customerGroup8ContentManage = this.lookupService(CustomerGroup8ContentManage.class);
			List<DataRow> records = customerGroup8ContentManage.getStatInfo();
			JSONArray jsonArray = new JSONArray();
			for(int i=0;i<records.size();i++){
				DataRow row = records.get(i);
				JSONObject jsonoObject = new JSONObject();
				jsonoObject.put("month",row.get("month"));
				jsonoObject.put("oppNums",row.get("oppNums")+"个");
				jsonoObject.put("orderNums",row.get("orderNums")+"个");
				jsonArray.put(jsonoObject);
			}
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("statInfoList", jsonArray);
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer getContentData(DataParam param){
		String responseText = FAIL;
		try {
			String date = param.getString("month");
			String mark = param.getString("mark");
			date = date+"-01";
			if("Last".equals(mark)){
				if(date.endsWith("01")){
					String tempDate = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL, DateUtil.getDateAdd(DateUtil.getDate(date),DateUtil.YEAR,-1));
					date = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL, DateUtil.getDateAdd(DateUtil.getDate(tempDate),DateUtil.MONTH,11));
				}else{
					date = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL, DateUtil.getDateAdd(DateUtil.getDate(date),DateUtil.MONTH,-1));
				}
			}else if("Next".equals(mark)){
				if(date.endsWith("12")){
					String tempDate = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL, DateUtil.getDateAdd(DateUtil.getDate(date),DateUtil.YEAR,1));
					date = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL, DateUtil.getDateAdd(DateUtil.getDate(tempDate),DateUtil.MONTH,-11));
				}else{
					date = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL, DateUtil.getDateAdd(DateUtil.getDate(date),DateUtil.MONTH,1));
				}
			}else if("Current".equals(mark)){
				date = DateUtil.getYear()+"-"+DateUtil.getMonth();
			}else{
			}
			date = date.substring(0,7);
			CustomerGroup8ContentManage customerGroup8ContentManage = this.lookupService(CustomerGroup8ContentManage.class);
			List<DataRow> oppRecords = customerGroup8ContentManage.getOppContentData(date);
			List<DataRow> orderRecords = customerGroup8ContentManage.getOrderContentData(date);
			JSONArray jsonArray1 = new JSONArray();
			for(int i=0;i<oppRecords.size();i++){
				DataRow dataRow = oppRecords.get(i);
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("id", dataRow.get("OPP_ID"));
				jsonObject.put("name", dataRow.get("OPP_NAME"));
				jsonObject.put("concernProduct", dataRow.get("OPP_CONCERN_PRODUCT"));
				jsonObject.put("expectInvest", dataRow.get("OPP_EXPECT_INVEST"));
				jsonObject.put("level", dataRow.get("OPP_LEVEL"));
				jsonObject.put("des", dataRow.get("OPP_DES"));
				jsonArray1.put(jsonObject);
			}
			JSONArray jsonArray2 = new JSONArray();
			for(int i=0;i<orderRecords.size();i++){
				DataRow dataRow = orderRecords.get(i);
				JSONObject jsonObject = new JSONObject();
				String id = dataRow.getString("ORDER_ID");
				jsonObject.put("id",id);
				jsonObject.put("name", dataRow.get("ORDER_NAME"));
				jsonObject.put("cost", dataRow.get("ORDER_COST"));
				jsonObject.put("datetime", dataRow.get("ORDER_CREATE_TIME"));
				List<DataRow> orderSubRecords = customerGroup8ContentManage.getOrderSubContentData(id);
				JSONArray jsonArray21 = new JSONArray();
				for(int j=0;j<orderSubRecords.size();j++){
					DataRow row = orderSubRecords.get(j);
					JSONObject jsonObject1 = new JSONObject();
					jsonObject1.put("name", row.get("ENTRY_ORDER_PRODUCT"));
					jsonObject1.put("num", row.get("ENTRY_NUMBER"));
					jsonObject1.put("price", row.get("ENTRY_UNIT_PRICE"));
					jsonObject1.put("discount", row.get("ENTRY_DISCOUNT"));
					jsonObject1.put("price", row.get("ENTRY_REAL_PRICE"));
					jsonArray21.put(jsonObject1);
				}
				jsonObject.put("subInfoList",jsonArray21);
				jsonArray2.put(jsonObject);
			}
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("oppInfoList", jsonArray1);
			jsonObject.put("orderInfoList", jsonArray2);
			jsonObject.put("month", date);
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		return new AjaxRenderer(responseText);
	}
}
