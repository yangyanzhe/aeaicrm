package com.agileai.crm.module.visit.service;

import com.agileai.crm.module.visit.service.VisitListSelect;
import com.agileai.hotweb.bizmoduler.core.PickFillModelServiceImpl;

public class VisitListSelectImpl
        extends PickFillModelServiceImpl
        implements VisitListSelect {
    public VisitListSelectImpl() {
        super();
    }
}
