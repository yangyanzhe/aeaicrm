package com.agileai.crm.module.taskreview.handler;

import java.util.Date;
import java.util.HashMap;

import com.agileai.crm.cxmodule.OrgInfoManage;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.controller.core.StandardEditHandler;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.domain.core.User;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.RedirectRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.util.DateUtil;
import com.agileai.util.StringUtil;

public class ReviewColdCallsEditHandler extends StandardEditHandler {
	protected HashMap<String, String> tabIdMapping = new HashMap<String,String>();
	protected String defaultTabId = "_base";
	public ReviewColdCallsEditHandler() {
		super();
		this.listHandlerClass = ReviewColdCallsListHandler.class;
		this.serviceId = buildServiceId(OrgInfoManage.class);
	}

	public ViewRenderer prepareDisplay(DataParam param) {
		String operaType = param.get(OperaType.KEY);
		if (isReqRecordOperaType(operaType)) {
			DataRow record = getService().getRecord(param);
			this.setAttributes(record);
		}
		if ("insert".equals(operaType)) {
			setAttribute("doEdit8Save", true);
		}
		if ("update".equals(operaType) || "detail".equals(operaType)) {
			DataRow record = getService().getRecord(param);
			if (record != null) {
				setAttribute("doEdit8Save", true);					
				if (record.get("ORG_STATE").equals("0")) {
					setAttribute("doRevokeApporve", true);
				} else {
					setAttribute("doApporve", true);
				}
			}
			this.setAttributes(record);
		}
		
		String currentSubTableId = param.get("currentSubTableId", defaultTabId);
		this.setAttribute("currentSubTableId", currentSubTableId);
		
		tabIdMapping.put(defaultTabId, "Layer0");
		tabIdMapping.put("visit", "Layer1");
		
		setAttribute("currentLayerleId", tabIdMapping.get(currentSubTableId));
		
		String date = DateUtil.getDateByType(
				DateUtil.YYMMDDHHMI_HORIZONTAL, new Date());
		this.setAttribute("ORG_UPDATE_TIME", date);
		this.setOperaType(operaType);
		processPageAttributes(param);
		return new LocalRenderer(getPage());
	}

	protected void processPageAttributes(DataParam param) {
		setAttribute("ORG_STATE", FormSelectFactory.create("PER_STATE")
				.addSelectedValue(getOperaAttributeValue("ORG_STATE", "0")));
		setAttribute("ORG_TYPE", FormSelectFactory.create("ORG_TYPE")
				.addSelectedValue(getOperaAttributeValue("ORG_TYPE", "")));
		setAttribute("ORG_CLASSIFICATION", FormSelectFactory.create("ORG_CLASSIFICATION")
				.addSelectedValue(getOperaAttributeValue("ORG_CLASSIFICATION", "")));
		setAttribute("ORG_SOURCES", FormSelectFactory.create("ORG_SOURCES")
				.addSelectedValue(getOperaAttributeValue("ORG_SOURCES", "")));
		User user = (User) this.getUser();
		this.setAttribute("ORG_CREATER_NAME",
				this.getAttribute("ORG_CREATER_NAME", user.getUserName()));
		this.setAttribute("ORG_CREATER",
				this.getAttribute("ORG_CREATER", user.getUserId()));
		this.setAttribute("ORG_SALESMAN_NAME",
				this.getAttribute("ORG_SALESMAN_NAME", user.getUserName()));
		this.setAttribute("ORG_SALESMAN",
				this.getAttribute("ORG_SALESMAN", user.getUserId()));
		String pcrtDate = (String) this.getAttribute("ORG_CREATE_TIME",
				DateUtil.getDateByType(DateUtil.YYMMDDHHMI_HORIZONTAL,
						new Date()));
		if (this.getAttribute("ORG_CREATE_TIME") == null) {
			this.setAttribute("ORG_CREATE_TIME", pcrtDate);
		}
    	this.setAttribute("currentSubTableId", param.get("currentSubTableId"));
    	setAttribute("currentSubTableId",getAttributeValue("currentSubTableId", "0"));
	}

	public ViewRenderer doSaveAction(DataParam param) {
		String operateType = param.get(OperaType.KEY);
		String responseText = SUCCESS;
		String orgName = param.get("ORG_NAME");
		String orgEmail = param.get("ORG_EMAIL");
		if (OperaType.CREATE.equals(operateType)) {
			DataParam repeatParam = new DataParam("ORG_NAME", orgName);
			DataRow record = getService().getRecord(repeatParam);
			
			boolean repeat = false;
			if(record!=null){
				if(!record.get("ORG_ID").equals(param.get("ORG_ID"))){
					repeat = true;
					responseText = "repeatName";
				}
			}
			if(!repeat){
				if (!StringUtil.isNullOrEmpty(orgEmail)) {
					DataParam repeatParam1 = new DataParam("ORG_EMAIL", orgEmail);
					DataRow record1 = getService().getRecord(repeatParam1);
					if(record1 != null){
						if(!record1.get("ORG_ID").equals(param.get("ORG_ID"))){
							repeat = true;
							responseText = "repeatMail";
						}
					}	
				} 
			}
			if (repeat){
				return new AjaxRenderer(responseText);
			}else{
				getService().createRecord(param);
			}
		} else if (OperaType.UPDATE.equals(operateType)) {
			DataParam repeatParam = new DataParam("ORG_NAME", orgName);
			DataRow record = getService().getRecord(repeatParam);
			
			boolean repeat = false;
			if(record != null){
				if(!record.get("ORG_ID").equals(param.get("ORG_ID"))){
					repeat = true;
					responseText = "repeatName";
				}
			}
			if (!repeat){
				if(!StringUtil.isNullOrEmpty(orgEmail)){
					DataParam repeatParam1 = new DataParam("ORG_EMAIL", orgEmail);
					DataRow record1 = getService().getRecord(repeatParam1);
					if(record1 != null){
						if(!record1.get("ORG_ID").equals(param.get("ORG_ID"))){
							repeat = true;
							responseText = "repeatMail";
						}
					}	
				}
			}
			if (repeat){
				return new AjaxRenderer(responseText);
			}else{
				getService().updateRecord(param);
			}
		}
		return new AjaxRenderer(responseText);
	}

	@PageAction
	public ViewRenderer doApporve(DataParam param) {
		param.put("ORG_STATE", "0");
		getService().changeStateRecord(param);
		return new RedirectRenderer(getHandlerURL(listHandlerClass));
	}

	@PageAction
	public ViewRenderer doRevokeApporve(DataParam param) {
		param.put("ORG_STATE", "1");
		getService().changeStateRecord(param);
		return new RedirectRenderer(getHandlerURL(listHandlerClass));
	}
	
	public ViewRenderer doAssignedSaleAction(DataParam param){
    	String orgId = param.get("ORG_ID");
		String orgSalesman = param.get("ORG_SALESMAN");
    	DataParam idParam = new DataParam();
    	idParam.put("ORG_ID", orgId);
    	idParam.put("ORG_SALESMAN", orgSalesman);
    	getService().assignedSaleRecord(idParam);
		return prepareDisplay(param);
	}
	
	public ViewRenderer doChangeSubTableAction(DataParam param) {
		return prepareDisplay(param);
	}
	
	protected OrgInfoManage getService() {
		return (OrgInfoManage) this.lookupService(this.getServiceId());
	}
}
