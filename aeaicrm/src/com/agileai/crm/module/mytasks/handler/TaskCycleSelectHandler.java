package com.agileai.crm.module.mytasks.handler;

import com.agileai.crm.module.mytasks.service.TaskCycleSelect;
import com.agileai.domain.DataParam;
import com.agileai.hotweb.controller.core.PickFillModelHandler;
import com.agileai.hotweb.domain.core.User;

public class TaskCycleSelectHandler
        extends PickFillModelHandler {
    public TaskCycleSelectHandler() {
        super();
        this.serviceId = buildServiceId(TaskCycleSelect.class);
    }

    protected void processPageAttributes(DataParam param) {
    }

    protected void initParameters(DataParam param) {
    	User user = (User) getUser();
    	String saleId = user.getUserId();
    	param.put("SALE_ID", saleId);
    }
    
    protected TaskCycleSelect getService() {
        return (TaskCycleSelect) this.lookupService(this.getServiceId());
    }
}
