package com.agileai.crm.module.opportunity.handler;

import java.util.List;

import com.agileai.crm.common.PrivilegeHelper;
import com.agileai.crm.cxmodule.OppInfoManage;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.controller.core.StandardListHandler;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.domain.core.User;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.DispatchRenderer;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;

public class MyCustOppInfoManageListHandler
        extends StandardListHandler {
    public MyCustOppInfoManageListHandler() {
        super();
        this.editHandlerClazz = MyCustOppInfoManageEditHandler.class;
        this.serviceId = buildServiceId(OppInfoManage.class);
    }
	public ViewRenderer prepareDisplay(DataParam param){
		User user = (User) this.getUser();
		PrivilegeHelper privilegeHelper = new PrivilegeHelper(user);
		if(privilegeHelper.isSalesDirector()){
			setAttribute("doConfirm", true);
		}else if(!privilegeHelper.isSalesDirector()){
			setAttribute("doConfirm", false);
		}
		mergeParam(param);
		initParameters(param);
		this.setAttributes(param);
		List<DataRow> rsList = getService().findRecords(param);
		this.setRsList(rsList);
		processPageAttributes(param);
		return new LocalRenderer(getPage());
	}
    protected void processPageAttributes(DataParam param) {
        setAttribute("OPP_STATE",
                     FormSelectFactory.create("OPP_STATE")
                                      .addSelectedValue(param.get("OPP_STATE")));
    	initMappingItem("OPP_STATE", FormSelectFactory.create("OPP_STATE")
				.getContent());
    }

    protected void initParameters(DataParam param) {
        initParamItem(param, "OPP_STATE", "");
        initParamItem(param, "OPP_NAME", "");
        initParamItem(param, "OPP_CONCERN_PRODUCT", "");
        initParamItem(param, "CUST_ID_NAME", "");
        initParamItem(param, "CLUE_SALESMAN_NAME", "");
    }
    @PageAction
    public ViewRenderer doCreateOrderAction(DataParam param) {
		storeParam(param);
		String url = "MyCustOppCreateOrderInfoEdit";
		return new DispatchRenderer(getHandlerURL(url) + "&"
				+ OperaType.KEY + "=doCreateOrderAction&comeFrome=doCreateOrderAction");
	}
    @PageAction
	public ViewRenderer doSubmit(DataParam param) {
		String oppId = param.get("OPP_ID");
		DataParam param1 = new DataParam("OPP_ID", oppId,"OPP_STATE","1");
		getService().changeStateRecord(param1);
		return prepareDisplay(param);
	}
    @PageAction
	public ViewRenderer doConfirm(DataParam param) {
		String oppId = param.get("OPP_ID");
		DataParam param1 = new DataParam("OPP_ID", oppId,"OPP_STATE","2");
		getService().changeStateRecord(param1);
		return prepareDisplay(param);
	}
    @PageAction
	public ViewRenderer doDispose(DataParam param) {
		storeParam(param);
		return new DispatchRenderer(getHandlerURL(this.editHandlerClazz) + "&"
				+ OperaType.KEY
				+ "=doDispose&comeFrome=doDispose");
	}
    
	public ViewRenderer doCheckCustStateAction(DataParam param){
		String responseText = "";
		DataRow dataRow = getService().getCustStateRecord(param);
		String custState = dataRow.getString("CUST_STATE");
		if(!"Confirm".equals(custState)){
			responseText = "客户状态为已确认的才可以新增商机";
		}
		return new AjaxRenderer(responseText);
	}
	
    protected OppInfoManage getService() {
        return (OppInfoManage) this.lookupService(this.getServiceId());
    }
}
